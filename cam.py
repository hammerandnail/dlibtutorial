from imutils import face_utils
import numpy as np
import dlib
import cv2
 
detector = dlib.simple_object_detector("detector.svm")
cam = cv2.VideoCapture(0)

tzuyu = cv2.imread('./tzuyu.png')

while(True):
    ret, img = cam.read()
    img = cv2.resize(img, (855, 480))
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    fists = detector(gray, 1)
 
    for fist in fists:
        (x, y, w, h) = face_utils.rect_to_bb(fist)
        # cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 3)
        try:
            img[y:y+h,x:x+w] = cv2.resize(tzuyu, (w, h))
        except:
            pass

 
    # print('SHOWING!!')
    img = cv2.flip(img,1)
 
    cv2.imshow('Fist', img)
    if cv2.waitKey(1)==ord('q'):
        break
 
cam.release()
cv2.destroyAllWindows()