import numpy as np
import dlib
import cv2

cam = cv2.VideoCapture(0)


idx = 1
while(True):
    ret, img = cam.read()
    print(img.shape)
    img = cv2.resize(img, (960, 540))

    cv2.imwrite('./data/{0:03d}.png'.format(idx),img)
    idx += 1

    img = cv2.flip(img,1)
 
    cv2.imshow('Fist', img)
    if cv2.waitKey(1)==ord('q'):
        break
 
cam.release()
cv2.destoryAllWindows()