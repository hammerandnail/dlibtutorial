import os
import glob
from PIL import Image
 
folder_path = './data'
img_list = glob.glob('{0}/*.jpg'.format(folder_path))

idx = 25
for img in img_list:
    im = Image.open(img)
    # im_name = img.split('/')[-1].split('.')[0]+'.png'
    im_name = str(idx)+'.png'
    if not os.path.isdir('{0}{1}'.format(folder_path,'_png')):
        os.system('mkdir {0}{1}'.format(folder_path,'_png'))
    im.save('{0}{1}/{2}'.format(folder_path,'_png', im_name))
    print('Convert img {0} in png format'.format(img.split('/')[-1]))
    idx += 1
 
print('Done!!')